// const display = document.querySelector("#display");
const result = document.querySelector("#result");
const display = document.querySelector("#currentState");
const buttons = document.querySelectorAll("button");
let equalPress = false;

buttons.forEach((item) => {
  item.addEventListener("click", (e) => {
    if (equalPress === true && item.className === "btn-number") {
      display.textContent = "";
      result.textContent = "";
      equalPress = false;
      console.log("toptin", item.className);
    }
    if (item.id == "clear") {
      display.innerHTML = "";
      result.innerHTML = "";

      // }else if(item.id == 'backspace'){
      // display.innerHTML = display.innerHTML.slice(0, -1);
      // let string = display.innerText.toString();
      // display.innerText = string.substr(0, string.length - 1);
    } else if (display.innerText != "" && item.id == "equal") {
      display.innerText = eval(result.innerText);
      equalPress = true;
      result.classList.add("active");
    } else if (display.innerText == "" && item.id == "equal") {
      display.innerText = "Empty";
      setTimeout(() => (display.innerText = ""), 2000);
    } else {
      result.classList.remove("active");

      let str = (display.innerText += item.id);
      equalPress = false;

      if (
        str.slice(-1) == "+" ||
        str.slice(-1) == "-" ||
        str.slice(-1) == "*" ||
        str.slice(-1) == "/" ||
        str.slice(-1) == "%" ||
        str.slice(-1) == "(" ||
        str.slice(-1) == ")" ||
        str.slice(-1) == "."
      ) {
        result.innerText = str.slice(0, -1);
      } else {
        result.innerText = eval(str);
      }
    }
  });
});


const themeToggleBtn = document.querySelector(".theme-toggler");

const calc = document.querySelector(".calc");

const toggleIcon = document.querySelector(".toggler-icon");

let isDark = true;
themeToggleBtn.addEventListener("click", () => {
  calc.classList.toggle("dark");
  themeToggleBtn.classList.toggle("active");
  isDark = !isDark;
});
